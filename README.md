# Recipes

[![pipeline status][pipeline_image]][pipeline]

[pipeline_image]: https://gitlab.com/rcousineau/recipes/badges/master/pipeline.svg
[pipeline]: https://gitlab.com/rcousineau/recipes/commits/master

This is a collection of family recipes.

https://rcousineau.gitlab.io/recipes/

---

This site is built using [mdBook][mdBook]. To build, first install mdBook with cargo:

```sh
$ cargo install mdbook
```

Then build it (the default output dir is `book`):

```sh
$ mdbook build
```

Or run a lightweight http server and watch for source changes:

```sh
$ mdbook serve
```

[mdBook]: https://github.com/rust-lang-nursery/mdBook
