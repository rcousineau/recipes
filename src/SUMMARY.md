# Summary

[About](./about.md)

-----------

- [Apps & Zerts](./apps_zerts.md)
    - [Bacon-Wrapped Smokies](./apps_zerts/bacon_wrapped_smokies.md)
    - [Cutout Cookies](./apps_zerts/cutout_cookies.md)
    - [Diced Potatoes](./apps_zerts/diced_potatoes.md)
    - [Russian Tea Cakes](./apps_zerts/russian_tea_cakes.md)
    - [Spritz](./apps_zerts/spritz.md)

- [Beverages](./beverages.md)
    - [Caramel Apple Spice](./beverages/caramel_apple_spice.md)
    - [Orange Julius](./beverages/orange_julius.md)
    - [Peanut Butter Banana Smoothie](./beverages/peanut_butter_banana_smoothie.md)

- [Breakfast](./breakfast.md)
    - [Bran Muffins](./breakfast/bran_muffins.md)
    - [Elephant Ears](./breakfast/elephant_ears.md)
    - [French Toast](./breakfast/french_toast.md)
    - [Hot Cross Muffins](./breakfast/hot_cross_muffins.md)
    - [Sour Cream Coffee Cake](./breakfast/sour_cream_coffee_cake.md)

- [Dinner](./dinner.md)
    - [Basil Parmesan Chicken](./dinner/basil_parmesan_chicken.md)
    - [Ham & Potato Soup](./dinner/ham_potato_soup.md)
    - [Miller Tacos](./dinner/miller_tacos.md)
    - [Mizithra & Brown Butter Pasta](./dinner/mizithra_brown_butter_pasta.md)
    - [Monte Cristo](./dinner/monte_cristo.md)
    - [Montreal Steak](./dinner/montreal_steak.md)
    - [Ratatouille Stew](./dinner/ratatouille_stew.md)
    - [Weiner Roll Ups](./dinner/weiner_roll_ups.md)
