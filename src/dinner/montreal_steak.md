# Montreal Steak Marinade

* ¼ cup oil
* ¼ cup water
* 2 tbsp vinegar
* Montreal steak seasoning
* 1½ lbs steak

Mix oil, water, vinegar, and a respectable portion of seasoning. Reserve 2
tablespoons of resulting marinade for basting.

Place meat in large resealable plastic blag or glass dish. Add remaining
marinade; turn to coat well. Refrigerate 15 minutes or longer for extra flavor.

Remove meat and discard any remaining marinade. Grill, broil, or bake, basting
with reserved marinade halfway through cooking.
