# Mizithra & Brown Butter Pasta

* 1 cup Mizithra cheese
* 1 cup butter
* 16 oz noodles

Browned Butter Procedure:

Cut butter into tablespoon-sized pieces and place in a 2-quart sauce pan. Place
the pan of butter on a burner on medium heat. Bring butter to a slow boil (about
5 minutes). Once the butter begins to boil, stir constantly to prevent residue
from sticking to the bottom of the pan. As the butter cooks, it will start to
foam and rise. Continue stirring, otherwise the butter foam could overflow
(about 5 minutes) and catch fire. Once the butter stops foaming and rising, cook
it until amber in color (about 1 to 2 minutes). It will have a pleasant caramel
aroma.

Boil pasta and drain. Sprinkle browned butter and cheese over the pasta.
