# Weiner Roll Ups

pie dough:

* 2 cups flour
* 1 cup Crisco shortening
* 1 tsp salt

Use a pastry blender to mix the above until they look like coarse crumbs.  Add
enough water to have it stick together, but not feel wet.  Start with a little,
add a little as needed.  When it makes a nice soft dough, roll it out between 2
pieces of wax paper, or on a cutting board with wax paper on top.

Spread mustard on the rolled out pie dough.  Cut the rolled out dough into 8
triangles. Place a cooked hot dog on the triangle and roll it up.

Bake at 400° for 20 minutes.
