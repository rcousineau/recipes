# Miller Tacos

* 1 lb ground beef
* ½ tsp salt & pinch of pepper
* 1 tsp Worscestershire sauce
* 1 tsp chili powder

Brown beef, drain grease. Combine ingredients and serve.
