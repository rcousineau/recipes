# Ratatouille Stew

* ¼ cup olive oil
* 1 large sweet onion
* 1 green bell pepper
* 1 red bell pepper
* 3 tsp minced garlic (or 6 cloves)
* 1 large eggplant
* 1 large zucchini
* 1 medium yellow squash
* 1 can diced tomatoes (14.5 oz)
* 2 tsp Italian seasoning
* 1½ tsp herbes de Provence (optional)
* 1 tsp salt
* 1 tsp black pepper
* ½ tsp dried thyme
* ½ cup vegetable broth
* 1 tbsp Worcestershire sauce
* 1 can tomato paste (6 oz)

Dice onion and bell peppers and place aside in medium bowl. Place minced garlic
aside in small bowl.

Slice eggplant into ½ inch disks then quarter. Slice zucchini into ¼ inch
disks then quarter. Slice yellow squash into ¼ inch disks then quarter. Place
eggplant, zucchini, and squash aside in large bowl. Drain diced tomatoes.

Pour oil into Instant Pot, press Sauté. Heat oil for 3 minutes on High. Add
onion and bell peppers and sauté for 3 minutes, stirring. Add minced garlic and
sauté for 1 minute.

Press Cancel and add eggplant, zucchini, yellow squash, and diced tomatoes. Add
Italian seasoning, salt, black pepper, dried thyme, vegetable broth, and
Worcestershire sauce. If using herbes de Provence, add that now.

Secure lid, ensure valve sealed, and press Keep Warm, then press Pressure Cook.
Set timer to 2 minutes. Quick release when done.

Stir in tomato paste. Let stand for 10 minutes to let thicken. Serve with grated
Parmesan cheese, if desired.

Vegetables will release a lot of fluid during pressure cooking. Stew will
thicken as it cools.
