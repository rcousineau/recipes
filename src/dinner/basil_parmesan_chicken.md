# Basil Parmsan Chicken

* 2 chicken breasts
* 1 zucchini
* 1 tsp olive oil
* 1 tsp garlic salt
* ¼ tsp salt
* ¼ cup parmesan cheese
* ¼ cup panko bread crumbs
* 2 tbsp butter
* 2 tbsp basil pesto
* 1 tsp lemon juice

Preheat oven to 450°.

Cut zucchini into ¼ inch slices. Combine with olive oil and garlic salt, then
spread on baking sheet lined with aluminum foil. Bake for 10 minutes.

Season both sides of chicken with a pinch of salt.

When zucchini is ready, place chicken on baking sheet and top with parmesan. Top
zuchhini with panko and parmesan. Bake for 20-25 minutes or until chicken
reaches internal temperature of 165°.

Combine butter, lemon juice, and pesto in a bowl. When chicken is ready, top
with pesto butter mixture.
