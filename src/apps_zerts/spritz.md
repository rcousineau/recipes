# Spritz

* 1 ½ cups butter
* 1 cup sugar
* 1 egg
* 2 tbsp milk
* 1 tsp vanilla
* ½ tsp almond extract
* 4 cups flour
* 1 tsp baking powder

Thoroughly cream butter and sugar. Add egg, milk, vanilla, and almond extract;
beat well. Sift flour and paking powder; add gradually to creamed mixture,
mixing till smooth. Do not chill. Force through cookie press onto ungreased
cookie sheet. Bake at 400° for 8 minutes.
