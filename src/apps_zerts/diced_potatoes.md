# Diced Potatoes

* 2 russet potatoes
* 2 tsp olive oil
* ¼ tsp salt
* minced garlic
* 1 oz grated parmesan

Prepare a baking sheet with foil and cooking spray.

Cut potatoes into ½ inch dice. Toss with olive oil, salt, and pinch of pepper.
Spreak into single layer on baking sheet.

Roast at 425° for 20 minutes.

Remove from oven. Toss with garlic and parmesan. Spread on baking sheet again.

Roast again for 10-15 minutes.
