# Christmas Butter Cookies - Marlene

* 1 cup butter
* 1 cup sugar
* 1 egg
* 1 tbsp milk
* 1 tsp vanilla
* 2¾ cups flour
* 1 tsp baking powder
* ¼ tsp salt

Cream butter gradually, add sugar, and beat until light and fluffy. Beat in egg,
milk, and vanilla. Combine flour, baking powder, and salt; gradually add to
creamed mixture. Chill for ease in handling. Roll out dough on lightly floured
surface to ⅛" thickness. Cut out cookies with floured cookie cutters. Bake at
350° for 8-10 minutes or until lightly browned.
