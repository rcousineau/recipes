# Bacon-Wrapped Smokies

## With Brown Sugar Glaze

* 1 pkg (16 oz) smokies
* 12 oz bacon
* ¾ cup brown sugar

Preheat oven to 400°. Line baking sheet with foil. Cut each bacon slice into
3-4 pieces. Wrap each piece around a smokie (a little overlap, not too tight).
Secure bacon to smokie with toothpick and place on baking sheet.

Sprinkle brown sugar over the wrapped and skewered smokies. Bake 15-20 minutes
or until bacon has browned and sugar has melted.
