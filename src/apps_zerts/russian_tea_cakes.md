# Russian Tea Cakes

* 1 cup butter
* ¼ cup powdered sugar
* 2 cup flour
* ¼ tsp salt
* 1 tsp vanilla
* ½ cup walnuts, chopped fine

In a mixing bowl, cream together butter and powdered sugar. Add flour, salt, and
vanilla; mix well. Add walnuts, then knead into a very stiff dough. Break off
small pieces of dough mixture and shape into small rolls. Place on greased
cookie sheet and bake for 350° for 10-12 minutes. Roll immediately in powdered
sugar until well coated.
