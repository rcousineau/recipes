# Apps & Zerts

### Terminology

**zerts**: desserts

**tre tres**: entrees (see: [Dinner](./dinner.md))

**sammies / sandoozles / Adam Sandlers**: sandwiches
(see: [Monte Cristo](./dinner/monte_cristo.md)

**cool blasterz**: A.C.

**big 'ol cookies**: cakes

**long-ass rice**: noodles
(see: [Mizithra & Brown Butter Pasta](./dinner/mizithra_brown_butter_pasta.md))

**fry fry chicky chick**: fried chicken

**chicky chicky parm parm**: chicken parm

**chicky catch**: chicken cacciatore

**pre-birds / future birds**: eggs

**super water**: root beer

**bean blankies**: tortillas

**food rakes**: forks

**go go mobile**: car
