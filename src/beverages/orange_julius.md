# Orange Julius

* 6 oz frozen OJ
* 1 cup water
* 1 cup milk
* ½ cup sugar
* 1 tsp vanilla
* 10-12 ice cubes

Mix in blender.
