# Peanut Butter Banana Smoothie

* 8 large ice cubes
* 1 cup of milk
* ½ cup vanilla yogurt
* ½ cup peanut butter
* 1 banana, sliced
