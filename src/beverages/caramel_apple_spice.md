# Caramel Apple Spice

Cinnamon Dolce Syrup:

* ¼ cup sugar
* ¼ cup brown sugar
* 1 tbsp flour
* ¼ tsp cinnamon
* 1 cup water
* 1 tsp vanilla

Whipped Cream:

* 1 cup heavy whipping cream
* 5 tbsp powdered sugar
* ½ tsp vanilla

Other:

* 32 oz apple juice
* caramel

---

Heat apple juice in large pot. Do not boil.

## Cinnamon Dolce Syrup

Mix sugar, brown sugar, flour, and cinnamon in small pot. Stir in water and
boil 2 minutes, then let cool and add vanilla. Store leftover syrup in jar.

---

## Whipped Cream

Beat whipping cream and powdered sugar for 2-3 minutes. Add vanilla.

---

Pour cinnamon dolce syrup into hot apple juice and stir. Top with whipped cream,
cinnamon, and caramel.
