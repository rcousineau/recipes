# Hot Cross Muffins

* 1⅔ cups flour
* ⅔ cup sugar
* 2 tsp baking powder
* ¾ tsp cinnamon
* ½ tsp salt
* 1 egg
* ⅓ cup vegetable oil
* ⅔ cup evaporated milk

Sift flour, sugar, baking powder, cinnamon, and salt into medium bowl. Create a
well in the center of the flour mixture. Beat together egg, oil, and evaporated
milk. Pour into the well. Stir with wooden spoon just until flour is moistened.
Spoon into muffin pan, filling cups ⅔ full. Bake at 400° for 20 minutes. Remove
from pan and cool for 5 minutes.

Frosting:

* ¼ cup powdered sugar
* 1½ tsp evaporated milk

Drizzle over the tops of muffins to form a cross.

Serve warm and enjoy!
