# Sour Cream Coffee Cake

* 1½ cups Bisquick
* ½ cup sugar
* 2 tbsp margarine (softened)
* 1 egg
* ¾ cup sour cream
* 1 tsp vanilla

Mix all of that, grease 9x9" square pan.

Topping:

* ¼ cup flour
* 3 tbsp sugar
* ⅛ tsp cinnamon
* 2 tbsp margarine (hard)

Bake at 350° for 35 minutes. Test with toothpick and enjoy!
