# Bran Muffins

* 1½ cups wheat bran
* 1 cup buttermilk
* ⅔ cup brown sugar
* ⅓ cup vegetable oil
* 1 egg
* ½ tsp vanilla extract
* 1 cup flour
* 1 tsp baking soda
* 1 tsp baking powder
* ½ tsp salt
* ½ cup raisins (optional)

Preheat oven to 375°. Grease muffin pan or line with paper cups. Mix wheat bran
and buttermilk in medium bowl and place aside. Let stand for 10 minutes.

Beat brown sugar, oil, egg, and vanilla together in mixing bowl until light and
fluffy. Stir in buttermilk mixture.

Sift flour, baking soda, baking powder, and salt together. Stir into bowl until
just blended. If desired, fold in raisins.

Spoon batter into muffin pan filling each cup ⅔ full. Bake 15 to 20 minutes.
Tops of muffins should spring back when pressed lightly.
