# Sue Miller's Elephant Ears

## (A.K.A. "the GOOD ones!")

* 3 cups flour
* 1 tsp baking powder
* 1 tsp salt
* 2 eggs
* ½ cup evaporated milk

Mix ingredients and knead dough. Roll out and cut out small biscuits. Deep fry
in Vegetable Oil until golden brown. Enjoy! ☺
