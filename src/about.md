# Recipes

This is a collection of family recipes.

👈 Use the nav to browse to the recipes.

If there is no nav, click the `☰` at the top of the page.

---

This is built using [mdBook](https://github.com/rust-lang-nursery/mdBook).

Source for the site is on [Gitlab](https://gitlab.com/rcousineau/recipes).
